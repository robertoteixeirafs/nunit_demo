﻿using Moq;
using NUnit.Calculator;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;
using TestDemo.Calculator;

namespace NUnit.UnitTests
{
    public class StringCalculator_UnitTests
    {
        private Mock<IStore> _mockStore;

        private StringCalculator GetCalculator()
        {
            _mockStore = new Mock<IStore>();
            var calc = new StringCalculator(_mockStore.Object);
            return calc;
        }

        [Test]
        public void Add_EmptyString_Return_0()
        {
            StringCalculator calc = GetCalculator();
            int expectedResult = 0;
            int result = calc.Add("");
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(3, "3")]
        [TestCase(6, "6")]
        [TestCase(9, "9")]
        public void Add_SingleNumbers_ReturnsTheNumber(int expected, string input)
        {
            StringCalculator calc = GetCalculator();            
            int result = calc.Add(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("2,3", 5)]
        [TestCase("101,20", 121)]
        [TestCase("3,8,10", 21)]
        [TestCase("1,2,3,4,5,6,7", 28)]
        public void Add_MultipleNumbers_ReturnSumOfAllNumbers(string input, int expectedResult)
        {
            StringCalculator calc = GetCalculator();
            int result = calc.Add(input);
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("a,1")]
        [TestCase("abc,''1")]
        [TestCase("qwerty")]
        [TestCase("-,/")]
        public void Add_InvalidString_ThrowsException(string input)
        {
            StringCalculator calc = GetCalculator();
            var ex = Assert.Catch<Exception>(() => calc.Add(input));
            ActualValueDelegate<int> testDelegate = () => calc.Add(input);
            
            Assert.That(testDelegate, Throws.TypeOf<ArgumentException>());
        }

        [TestCase("2")]
        [TestCase("5,6")]
        [TestCase("3,4")]
        [TestCase("10,10,3")]
        [TestCase("5,5,5,5,5,5,5,5,5,5,3")]
        public void Add_ResultIsAPrimeNumber_ResultsAreSaved(string input)
        {
            StringCalculator calc = GetCalculator();

            var result = calc.Add(input);

            _mockStore.Verify(m => m.Save(It.IsAny<int>()), Times.Once);
        }
    }

}
