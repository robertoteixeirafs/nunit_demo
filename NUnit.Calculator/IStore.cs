﻿namespace TestDemo.Calculator
{
    public interface IStore
    {
        void Save(int result);
    }
}
